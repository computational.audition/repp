# Repp package legacy code



## Important note:
**We are sorry, the location of the repp package has changed!! please use this location: 
https://gitlab.com/computational-audition/repp**



# REPP: online SMS experiments

**Manuel Anglada-Tort, Peter Harrison, and Nori Jacoby**\
[Computational Auditory Perception Group](https://www.aesthetics.mpg.de/en/research/research-group-computational-auditory-perception.html), \
Max Planck Institute for Empirical Aesthetics.


_REPP (Rhythm ExPeriment Platfornm)_ is a python package for measuring
sensorimotor synchronization (SMS) in laboratory and online settings.

## Documentation

REPP documentation: https://computational-audition.gitlab.io/repp/

Source code: https://gitlab.com/computational-audition/repp


## Citation

Please cite this package if you use it:

Anglada-Tort, M., Harrison, P.M.C. & Jacoby, N. REPP: A robust cross-platform solution for online sensorimotor synchronization experiments. Behavior Reseasrch Methods (2022). https://doi.org/10.3758/s13428-021-01722-2


**Tapping datasets** supporting the paper (2022): https://osf.io/r2pxd/


## REPP Contributors

**Vani Rajendran**: developed analysis and plotting methods for beat detection tasks (see `beatfinding_cyclic` demo).


## Installation (macOS)
 See in the updated code location: https://gitlab.com/computational-audition/repp


## Contributing

We welcome contributions supporting new paradigms or improvements in the current code. Please talk to one of the authors if you would like to contribute.


## License

 MIT License

